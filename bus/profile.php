<?php

session_start();



?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Bus Management | My Profile</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/cover1.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="../../assets/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>
<nav class="navbar navbar-default navbar-static-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.html">PSIT BUS MANAGEMENT</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li><a href="home.php">Home</a></li>
            <li><a href="about.php">About</a></li>
            <li><a href="contact.php">Contact</a></li>
           
<?php
             if(isset($_SESSION['valid']) && $_SESSION['valid']=="valid" && isset($_SESSION['role']) && $_SESSION['role']=="admin" ){ 
            ?>

           
     <li ><a  href="admin_search.php" >Search</a></li>
                   
<?php
}
?>


           <!-- <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Dropdown <span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="#">Action</a></li>
                <li><a href="#">Another action</a></li>
                <li><a href="#">Something else here</a></li>
                <li class="divider"></li>
                <li class="dropdown-header">Nav header</li>
                <li><a href="#">Separated link</a></li>
                <li><a href="#">One more separated link</a></li>
              </ul>
            </li> -->
          </ul>
          <ul class="nav navbar-nav navbar-right">
             <?php
             if($_SESSION['valid']=="valid"){ 
              echo '<li class="active"><a href="profile.php">Howdy, '.$_SESSION["name"].'</a></li><li><a href="logout.php">Logout</a></li>';

          }
          else{

              echo '<li><a href="signup.php">Sign up</li><li><a href="login.php">Login</a></li>';


          }

          ?>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>


    <div class="container">

      <!-- Main component for a primary marketing message or call to action -->
      


<?php

if($_SESSION['valid']=="valid")
{

  ?>


<h2>My Profile</h2>
<table class="table table-bordered table-condensed table-striped">

<?php 
include 'include.php';
$id = $_SESSION['id'];
$sql = "SELECT * FROM user where id='$id'";
$result = mysqli_query($conn, $sql);
//echo $id."---->";

if (mysqli_num_rows($result) > 0) {
    // output data of each row
   //echo "printing";
    while($row = mysqli_fetch_assoc($result)) {
    //echo "came here";
    echo '<tr><Td rowspan="10"><img src="uploads/'.$row["uid"].'" class="img-thumbnail" style="height:200px;width:200px;"alt="Responsive image">';



    if($row['role']=="Professor"){
    echo "<tr><th>Unique Number<td>F".$row['branch'].$row['year'].$row['id'];
    }
    else{
echo "<tr><th>Unique Number<td>S".$row['branch'].$row['year'].$row['id'];

    }
    echo "<tr><th>Name<td>".$row['name'];
    echo "<tr><th>Email<td>".$row['email'];
    echo "<tr><th>Address<td>".$row['address'];
    echo "<tr><th>Phone<td>".$row['phone'];
    echo "<tr><th>Date of Birth<td>".$row['dateofbirth'];
    echo "<tr><th>Gender<td>".$row['gender'];
    echo "<tr><th>Branch<td>".$row['branch'];
    echo "<tr><th>Year<td>".$row['year'];
       
 
    }

}
?>





</table>
<?php
}

else{


?>
Please <a href="login.php">Login</a> to Access this page

<?php


}



?>
<div class="mastfoot">
            <div class="inner">
              <p>Created and maintained by PSIT,Kanpur.</p>
            </div>
          </div>


    </div> <!-- /container -->






    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="../../assets/js/docs.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
