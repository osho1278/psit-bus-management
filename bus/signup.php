<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Bus Management | Login</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/cover1.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="../../assets/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
<script>

function radio_called(role){


//alert("radion");


if(role=="Student" || role=="Professor"){
document.getElementById("branch").innerHTML='<div class="form-group"><label for="inputEmail3" class="col-sm-2 control-label">Branch</label>  <div class="col-sm-10"> <label class="radio-inline">  <input type="radio" name="branchOption" id="inlineRadio1" value="CS">CS</label><label class="radio-inline">  <input type="radio" name="branchOption" id="inlineRadio1" value="CE">CE</label><label class="radio-inline">  <input type="radio" name="branchOption" id="inlineRadio1" value="EC">EC</label><label class="radio-inline">  <input type="radio" name="branchOption" id="inlineRadio1" value="EN">EN</label><label class="radio-inline">  <input type="radio" name="branchOption" id="inlineRadio1" value="IT">IT</label><label class="radio-inline">  <input type="radio" name="branchOption" id="inlineRadio1" value="ME">ME</label></div></div>';


}
else{

document.getElementById("branch").innerHTML='';

}





}


function form_validation()
{

//phone validation
var phone = document.getElementById('phone').value;
//var phone ="123123";
//alert(phone);
if(phone.length ==0 || phone.length !=10 || isNaN(phone))
{
alert("Check phone number");

return false;

}






//password validation
var inputtxt = document.getElementById('password');
var paswd=  /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{7,15}$/;
if(inputtxt.value.match(paswd)) 
{ 
//alert('Correct, try another...');
//return true;
}
else
{ 
alert('Your password does not meets our minimum security criteria!');
return false;
}





var name = document.getElementById('name').value;
//var phone ="123123";
//alert(name);
if(name.length ==0 || !isNaN(name))
{
alert("Check your name");

return false;

}









return true;



}



</script>








  </head>

  <body>
     <nav class="navbar navbar-default navbar-static-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.html">PSIT BUS MANAGEMENT</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li ><a href="home.php">Home</a></li>
            <li><a href="#about">About</a></li>
            <li><a href="#contact">Contact</a></li>
            <!-- <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Dropdown <span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="#">Action</a></li>
                <li><a href="#">Another action</a></li>
                <li><a href="#">Something else here</a></li>
                <li class="divider"></li>
                <li class="dropdown-header">Nav header</li>
                <li><a href="#">Separated link</a></li>
                <li><a href="#">One more separated link</a></li>
              </ul>
            </li> -->
          </ul>
          <ul class="nav navbar-nav navbar-right">
          <li class="active"><a href="#">Sign up</a></li>
          <li ><a href="login.php">Login</a></li>

          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>



<div class="container">
<div class="col-sm-2"> </div>
<div class="col-sm-8">

  <?php
  
if(isset($_GET['message']) && $_GET['message']){

?>

 <div class="alert alert-danger" role="alert"><?php echo "<strong>Whoops !! Something went wrong .</strong>".$_GET['message']; ?></div>
<?php
}
else
{


 echo '  <div class="alert alert-info" role="alert"><strong>Heads up !! </strong>Please fill the details correctly or else you may be banned !!</div>';
}
?>




<div class="panel panel-primary">
  <div class="panel-heading">
                <p class="lead">Please enter details to become our valued customer.</p>



  </div>
  <div class="panel-body" style="background-color:grey1">
   

<form class="form-horizontal" method="POST" action="signup_backend.php" enctype="multipart/form-data" onsubmit= "return form_validation()">
  

<div class="form-group">
    <label for="inputEmail3" class="col-sm-2 control-label">College Id</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" name= "uid" id="inputEmail3" placeholder="College Id">
    </div>
  </div>



  <div class="form-group">
    <label for="inputEmail3" class="col-sm-2 control-label">Name</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" name= "name" id="name" placeholder="Name">
    </div>
  </div>


  <div class="form-group">
    <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
    <div class="col-sm-10">
      <input type="email" class="form-control" id="inputEmail3" name ="email" placeholder="Email">
    </div>
  </div>
  <div class="form-group">
    <label for="inputPassword3" class="col-sm-2 control-label">Password</label>
    <div class="col-sm-10">
      <input type="password"  name = "password" class="form-control" id="password" placeholder="Password">
    </div>
  </div>

 <div class="form-group">
    <label for="inputEmail3" class="col-sm-2 control-label">Address</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" name="address"id="inputEmail3" placeholder="Address">
    </div>
  </div>
 <div class="form-group">
    <label for="inputEmail3" class="col-sm-2 control-label">Phone Number</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="phone" name="phone" placeholder="Phone Number">
    </div>
  </div>

 <div class="form-group">
    <label for="inputEmail3" class="col-sm-2 control-label">Role</label>
    <div class="col-sm-10">

<label class="radio-inline">
  <input type="radio" name="RadioOptions" id="inlineRadio1" value="Student" onclick="return radio_called('Student')"> Student
</label>
<label class="radio-inline">
  <input type="radio" name="RadioOptions" id="inlineRadio2" value="Professor" onclick="return radio_called('Professor')"> Professor
</label>
<label class="radio-inline">
  <input type="radio" name="RadioOptions" id="inlineRadio3" value="Staff" onclick="return radio_called('Staff')"> Staff
</label>
</div>
</div>

<div id="branch">

</div>



<div class="form-group">
    <label for="inputEmail3" class="col-sm-2 control-label">Gender</label>
    <div class="col-sm-10">

<label class="radio-inline">
  <input type="radio" name="GenderOptions" id="inlineRadio1" value="Male"> Male
</label>
<label class="radio-inline">
  <input type="radio" name="GenderOptions" id="inlineRadio2" value="Female"> Female
</label>
</div>
</div>


<div class="form-group">
    <label for="inputEmail3" class="col-sm-2 control-label">Date of Birth</label>
    <div class="col-sm-10">
  <input type="date" name="dob" id="inlineRadio3" >

</div>
</div>


 <div class="form-group">
    <label for="exampleInputFile" class="col-sm-2 control-label">File input</label>
    <div class="col-sm-10">
    <input type="file" name="fileToUpload" id="fileToUpload">

    <p class="help-block">Upload your image here. (only jpg,jpeg,png accepted)</p>
    </div>
  </div>


  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <button type="submit" class="btn btn-success btn-lg" onclick="return form_validation();">Sign up</button>
            <a href = "index.html"><button  class="btn btn-danger btn-lg">Cancel</button></a>
    </div>
  </div>




</form> 

</div>
</div>
          <div class="mastfoot">
            <div class="inner">
              <p>Created and maintained by PSIT,Kanpur.</p>
            </div>
          </div>

        </div>

      </div>

    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="../../dist/js/bootstrap.min.js"></script>
    <script src="../../assets/js/docs.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
