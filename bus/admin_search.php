<?php

session_start();



?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Bus Management | Home</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/cover1.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="../../assets/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>
<nav class="navbar navbar-default navbar-static-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.html">PSIT BUS MANAGEMENT</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li><a href="home.php">Home</a></li>
            <li><a href="about.php">About</a></li>
            <li><a href="contact.php">Contact</a></li>
  <?php
             if($_SESSION['valid']=="valid" && $_SESSION['role']=="admin"){ 
            ?>

           
     <li class="active"><a  href="admin_search.php" >Search</a></li>
                   
<?php
}
?>


          </ul>
          <ul class="nav navbar-nav navbar-right">
             <?php
             if($_SESSION['valid']=="valid"){ 
              echo '<li><a href="profile.php">Howdy, '.$_SESSION["name"].'</a></li><li><a href="logout.php">Logout</a></li>';

          }
          else{

              echo '<li><a href="signup.php">Sign up</li><li><a href="login.php">Login</a></li>';


          }

          ?>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>


    <div class="container">

      <!-- Main component for a primary marketing message or call to action -->
      


<?php
if($_SESSION['role']=="admin")
{

?>


<form class="form-inline" method="POST" action="admin_search.php">

<div class="form-group">
    <label class="sr-only" for="exampleInputEmail3">NAME</label>
    <input type="text" class="form-control" id="exampleInputEmail3" name="name" placeholder="Name">
  </div>
  <div class="form-group">
    <label class="sr-only" for="exampleInputPassword3">Email</label>
    <input type="email" class="form-control" id="exampleInputPassword3" name="email" placeholder="Email">
  </div>
<div class="form-group">
    <label class="sr-only" for="exampleInputPassword3">Year</label>
    <input type="text" class="form-control" id="exampleInputPassword3" name="year" placeholder="Year">
  </div>



  <button type="submit" class="btn btn-success btn-sm" >Search</button>



</form>
<br>
<?php

}


if($_SESSION['valid']=="valid")
{




  ?>

<div class="alert alert-info" role="alert">Searching:
<?php
if(isset($_POST['name']) && $_POST['name']!=''){
echo "NAME -- '".$_POST['name']."' ";
}
if(isset($_POST['email']) && $_POST['email']!=''){
echo "EMAIL -- '".$_POST['email']."' ";
}
if(isset($_POST['year']) && $_POST['year']!=''){
echo "YEAR -- '".$_POST['year']."' ";
}




?>







</div>

<table class="table table-bordered">
<tr><th>S No.</th><th>NAME</th><th>EMAIL</th><th>ADDRESS</th><th>PHONE</th><th>GENDER</th><th>BRANCH</th><th>YEAR</th></tr>

<?php 
include 'include.php';
$sql = "SELECT * FROM user";
$result = mysqli_query($conn, $sql);
$id=1;
if (mysqli_num_rows($result) > 0) {
    // output data of each row
    while($row = mysqli_fetch_assoc($result)) {
       $correct=0;
        if(isset($_POST['name'])){
            if($_POST['name']==$row['name']){
              $correct=1;
             
            }
          
        else{
          $correct=0;
        }
}
       if(isset($_POST['email']) && $_POST['email']!=''){
            if($_POST['email']==$row['email']){
              $correct=1;
              }
            else{
              $correct=0;
              
              }
            }

       if(isset($_POST['year']) && $_POST['year']!=''){
            if($_POST['year']==$row['year']){
              $correct=1;
              }
            else{
              
              $correct=0;
              }
            }

  
if($correct==1){
        echo "<tr><td>".$id."<td>".$row["name"]."<td>".$row["email"]."<td>".$row["address"]."<td>".$row["phone"]."<td>".$row["gender"]."<td>".$row["branch"]."<td>".$row["year"];
        $id+=1;
    }}
  }


?>





</table>
<?php
}

else{


?>
Please <a href="login.php">Login</a> to Access this page

<?php
}

?>


    </div> <!-- /container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="../../assets/js/docs.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>