<?php

session_start();



?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Bus Management | Home</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/cover1.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="../../assets/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

    <![endif]-->

<script>

function other_options(a,b,c,d,e){
//intermediate id origin destination time

var res = a.split(","); 
  
document.getElementById('form_details').innerHTML="You have selected the following bus :<table class='table table-bordered'><tr><th>Bus Id<th>Origin<th>Destination<th>Time <tr class='danger'><Td>"+b+"<Td>"+c+"<Td>"+d+"<Td>"+e+"</table><h6>Please select the Intermediate station:";

var str ='';
for(i=0;i<res.length;i++){
str =str+ '<label class="radio-inline"><input type="radio" name = "interstation" id="intermediate" value="'+res[i]+'">'+res[i]+'</label>';
} 

str = str+'<input type="hidden" value="'+b+'" name="busid"><hr><img src="maps/'+c+d+'" class="image-rounded" style="width:550px;height:300px">'


document.getElementById('form_inter').innerHTML=str;


$('#modalforselection').modal('show');
return false;


}

</script>




  </head>

  <body>
<nav class="navbar navbar-default navbar-static-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.html">PSIT BUS MANAGEMENT</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="#">Home</a></li>
            <li ><a href="about.php">About</a></li>
            <li><a href="contact.php">Contact</a></li>

 <?php
             if(isset($_SESSION['valid']) && isset($_SESSION['role']) && $_SESSION['valid']=="valid" && $_SESSION['role']=="admin"){ 
            ?>

            
     <li><a href="admin_search.php" >Search</a></li>
                   
<?php
}
?>

          </ul>
          <ul class="nav navbar-nav navbar-right">
             <?php
             if(isset($_SESSION['valid']) && $_SESSION['valid']=="valid"){ 
              echo '<li><a href="profile.php">Howdy, '.$_SESSION["name"].'</a></li><li><a href="logout.php">Logout</a></li>';

          }
          else{

              echo '<li><a href="signup.php">Sign up</li><li><a href="login.php">Login</a></li>';


          }

          ?>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>


    <div class="container">

      <!-- Main component for a primary marketing message or call to action -->
      


<?php
if(isset($_SESSION['role']) && $_SESSION['role']=="admin"){

if(isset($_GET['message']) &&  $_GET['message']!='')
{

echo '<div class="alert alert-danger" role="alert">'.$_GET['message'].'</div>';
}
?>


<div class="col-sm-10">
<div class="alert alert-info" role="alert">Displaying the list of buses : <h5> <code>RED : FILLED</code>  <code>GREEN : EMPTY</code> <code>WHITE : FILLING FAST</code> </h4><h6>** Bus once selected cannot be changed back.</h6></div>
</div>

<div class="col-sm-2">

<button type="button" class="btn btn-primary btn-lg btn-block" data-toggle="modal" data-target="#myModal">
  Add a Bus
</button>
</div>



<?php


}


  ?>


<?php
if(!isset($_SESSION['valid']) || $_SESSION['valid']!="valid")
{
?>
Please <a href="login.php">Login</a> to Access this page<hr>
<?php
}
?>



<table class="table table-bordered">
<tr><th>S No.</th><th>Origin</th><th>Destination</th><th>Intermediate Stations</th><th>Time</th><th>Seats Left</th><th>Select</th></tr>

<?php 
include 'include.php';
$sql = "SELECT * FROM buses";
$result = mysqli_query($conn, $sql);
$id=1;
if (mysqli_num_rows($result) > 0) {
    // output data of each row
  
    while($row = mysqli_fetch_assoc($result)) {
      $filled =0;
        if($row['seats']==$row['seatsoccupied']){
echo "<tr class='danger'>";
  $filled=1;
        }
else if($row['seats']-$row['seatsoccupied'] < 8){
  echo "<tr class='warning'>";

}        
else{
echo "<tr class='success'>";

}
        echo "<td>".$id."<td>".$row["origin"]."<td>".$row["destination"]."<td>".$row["intermediate"]."<td>".$row["time"]."<td>".($row["seats"]-$row["seatsoccupied"]);

if(isset($_SESSION['valid']) && $_SESSION['valid']=="valid" && $filled==0){
        echo "<td><input type='radio' name='busselect' onclick='return other_options(\"".$row["intermediate"]."\",\"".$row['id']."\",\"".$row['origin']."\",\"".$row['destination']."\",\"".$row['time']."\")'>";
        }
else{
echo "<td>";

}

        $id+=1;
    }

}
?>





</table>


    </div> <!-- /container -->



<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Create a bus</h4>
      </div>
      <div class="modal-body">
  
    <form class="form-horizontal" enctype="multipart/form-data" method="post" action ="add_bus.php">
  
  <div class="form-group">
    <label for="inputEmail3" class="col-sm-2 control-label">Origin</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" name = "origin" id="origin" placeholder="Origin">
    </div>
  </div>
  <div class="form-group">
    <label for="inputPassword3" class="col-sm-2 control-label">Destination</label>
    <div class="col-sm-10">
      <input type="text" class="form-control"  name="destination" id="destination" placeholder="Destination">
    </div>
  </div>
  <div class="form-group">
    <label for="inputPassword3" class="col-sm-2 control-label">Intermediate Stations</label>
    <div class="col-sm-10">
      <input type="text" class="form-control"  name="stations" id="destination" placeholder="Intermediate Destination">
    </div>
  </div>
  <div class="form-group">
    <label for="inputPassword3" class="col-sm-2 control-label">Time</label>
    <div class="col-sm-10">
      <input type="text" class="form-control"  name="time" id="time" placeholder="Time">
    </div>
  </div>
 <div class="form-group">
    <label for="inputPassword3" class="col-sm-2 control-label">Total Seats</label>
    <div class="col-sm-10">
      <input type="text" class="form-control"  name="seats" id="seats" placeholder="Total seats">
    </div>
  </div>

<div class="form-group">
    <label for="exampleInputFile" class="col-sm-2 control-label">File input</label>
    <div class="col-sm-10">
    <input type="file" name="fileToUpload" id="fileToUpload">

    <p class="help-block">Upload your image here. (only jpg,jpeg,png accepted)</p>
    </div>
  </div>








      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
</form> 
      

      </div>
    </div>
  </div>
</div>


<!-- /*modal for bus selection*/
 -->



<div class="modal fade" id="modalforselection" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style="background-color:rgb(50,50,50); color:white">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel" style="background-color:rgb(50,50,50)">Please Confirm your booking</h4>
      </div>
      <div class="modal-body" id="modalBody">
  <div id="form_details">
  </div>
  

    <form class="form-inline" method="post" action ="confirm_bus.php">
  <div id="form_inter">
  </div>
  






      </div>
      <div class="modal-footer" style="background-color:rgb(50,50,50); color:white">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-success">Save change</button>
</form> 
      

      </div>
    </div>
  </div>
</div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="../../assets/js/docs.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
